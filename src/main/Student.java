package main;
import java.time.LocalDate;


/*
  1.All the fields should be private
  2.there must be at least one LocalDate field
  3.must define parameterized constructor
  4.must override toString method
  5.use StringBuilder in toString method
  6.must implement Comparable interface
  7.generate getters , setters equals methods from eclipse
  8.must add an enum field
  
 * 
 */
public class Student {
public int getRollNumber() {
		return rollNumber;
	}



	public void setRollNumber(int rollNumber) {
		this.rollNumber = rollNumber;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}



	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}



	public double getMark() {
		return mark;
	}



	public void setMark(double mark) {
		this.mark = mark;
	}



	public Gender getGender() {
		return gender;
	}



	public void setGender(Gender gender) {
		this.gender = gender;
	}



private int rollNumber;
private String name;
private LocalDate dateOfBirth;
private double mark;
private Gender gender;



public Student(int rollNumber, String name, LocalDate dateOfBirth, double mark, Gender gender) {
	super();
	this.rollNumber = rollNumber;
	this.name = name;
	this.dateOfBirth = dateOfBirth;
	this.mark = mark;
	this.gender = gender;
}



}

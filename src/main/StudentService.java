package main;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

/**
 * A interface specifies the operations on Student object
 * @author tinu
 *
 * 
 */
public interface StudentService {
	/**
	 * The method  will add a student
	 * @param student  student to be added
	 * 
	 */	
 void addStudent(Student student);

/**
 * The method  should display all the students
  */	
 void displayAll();
 

/**
 * The method will search students and display the student with given name
 * @param name name of the student to be searched
 * 
 */
 void search(String name);

/**
 * The method will search students and display the student with DOB is after the given DOB
 * @param dateOfBirth Date of birth
 * 
 */
List<Student>search(LocalDate dateOfBirth);

/**
 * The method  sorts the students  based on name
 */
 void sort();
 
 /**
  * The method  deletes all the  the students  based on name
  */
  void delete(String name);
  
 //this method accept a student and returns the details as CSV String
 default String getCSVString(Student student)
 {
	 
 return new StringBuffer(student.getName())
		 .append(",")
		 .append(student.getRollNumber()).append(",")
		 .toString();
 //csv must contain all the fields
 //all the values except of numeric type should be enclosed in single quotes
 //for example "tom",30,5000 ,"mg road,kochi"
 }

 default String getJsonString(Student student)
 {   //use StringBuffer
	 //json must contain all the fields
	 //all the values except of numeric type should be enclosed in single quotes
	 //for example {"name":"Abin","age":27}
 
   return null;
 }
 void downloadDetailsAsCSV() throws IOException;
 void downloadDetailsAsJson()throws IOException;
}
